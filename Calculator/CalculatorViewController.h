//
//  CalculatorViewController.h
//  Calculator
//
//  Created by Felipe Venegas on 21/04/13.
//  Copyright (c) 2013 Felipe Venegas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *display;

@end
