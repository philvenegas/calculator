//
//  CalculatorViewController.m
//  Calculator
//
//  Created by Felipe Venegas on 21/04/13.
//  Copyright (c) 2013 Felipe Venegas. All rights reserved.
//

#import "CalculatorViewController.h"
#import "CalculatorBrain.h"

// private properties
@interface CalculatorViewController ()

@property (nonatomic) BOOL userIsInTheMiddleOfEnteringANumber;
@property (nonatomic, strong) CalculatorBrain *brain;

@end

@implementation CalculatorViewController

@synthesize display = _display;
@synthesize userIsInTheMiddleOfEnteringANumber = _userIsInTheMiddleOfEnteringANumber;
@synthesize brain = _brain;

- (CalculatorBrain *)brain
{
    if (!_brain) {
        _brain = [[CalculatorBrain alloc] init];
    }
    return _brain;
}

// IBAction typedef to void
// id is a type pointer to any kind of object
//      or unkown class of object
- (IBAction)digitPressed:(UIButton *)sender
{
    NSString *digit = sender.currentTitle;

    // send digit to console : %@ is an object
    // send msg description / return NSString
    // NSLog(@"digit pressed = %@", digit);
    
    if (self.userIsInTheMiddleOfEnteringANumber) {
        self.display.text = [self.display.text stringByAppendingString:digit];
    } else {
        self.display.text = digit;
        self.userIsInTheMiddleOfEnteringANumber = YES;
    }

//    NSString *currentText = myDisplay.text; //[myDisplay text];
//    NSString *newText  = [currentText stringByAppendingString:digit];
//    myDisplay.text = newText; //[myDisplay setText:newText];

}

- (IBAction)enterPressed
{
    [self.brain pushOperand:[self.display.text doubleValue]];
    self.userIsInTheMiddleOfEnteringANumber = NO;
}


- (IBAction)operationPressed:(UIButton *)sender
{
    if (self.userIsInTheMiddleOfEnteringANumber) {
        [self enterPressed];
    }
    double result = [self.brain performOperation:sender.currentTitle];
    NSString *resultString = [NSString stringWithFormat:@"%g", result];
    self.display.text = resultString;
}

@end
