//
//  CalculatorBrain.m
//  Calculator
//
//  Created by Felipe Venegas on 21/04/13.
//  Copyright (c) 2013 Felipe Venegas. All rights reserved.
//

#import "CalculatorBrain.h"

@interface CalculatorBrain()
@property (nonatomic, strong) NSMutableArray *operandStack;
@end

@implementation CalculatorBrain
// @synthesize never allocates
@synthesize operandStack = _operandStack;

// getter operandStack
- (NSMutableArray *)operandStack {
    // lazy instaniation
    if (_operandStack == nil) {
        _operandStack = [[NSMutableArray alloc] init];
    }
    return _operandStack;
}
// setter operandStack
// - (void)setOperandStack:(NSMutableArray *)operandStack {
//     _operandStack = operandStack;
// }

- (void)pushOperand:(double)operand {
    // self.operandStack can be nil as @property (nonatomic, strong) NSMutableArray *operandStack;
    // starts of as a nil and calling self.operandStack does nothing... so lazy instaniation in getter fn
    [self.operandStack addObject:[NSNumber numberWithDouble:operand]];
}


- (double)popOperand
{
    NSNumber *operandObject = [self.operandStack lastObject];
    if (operandObject) {
        [self.operandStack removeLastObject];
    }
    return [operandObject doubleValue];
}

- (double)performOperation:(NSString *)operation {

    double result = 0;

    if ([operation isEqualToString:@"+"]) {
        result = [self popOperand] + [self popOperand];
    }
    else if ([@"*" isEqualToString:operation]) {
        result = [self popOperand] * [self popOperand];
    }

    [self pushOperand:result];
    
    return result;
}

@end
