//
//  CalculatorAppDelegate.h
//  Calculator
//
//  Created by Felipe Venegas on 21/04/13.
//  Copyright (c) 2013 Felipe Venegas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
