//
//  CalculatorBrain.h
//  Calculator
//
//  Created by Felipe Venegas on 21/04/13.
//  Copyright (c) 2013 Felipe Venegas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorBrain : NSObject

- (void)pushOperand:(double)operand;
- (double)performOperation:(NSString *)operation;

@end
